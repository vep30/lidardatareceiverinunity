using System;
using UnityEngine;
using System.Collections.Generic;

public class UnityMainThreadDispatcher : MonoBehaviour
{
    public static UnityMainThreadDispatcher instance;
    
    private Queue<Action> actionQueue = new ();
    
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }
    
    void Update()
    {
        while (actionQueue?.Count > 0)
            actionQueue.Dequeue().Invoke();
    }
    
    public void Enqueue(Action action)
    {
        instance.actionQueue.Enqueue(action);
    }
}