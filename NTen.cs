using System;
using UnityEngine;
using System.IO.Ports;
using Scripts.Calibration;
using System.Threading.Tasks;
using System.Collections.Generic;

// скрипт получения и обработки данных с датчика Lidar N10

public class NTen : MonoBehaviour
{
    public static NTen instance;
    
    [SerializeField] private GameObject pointPrefab; // префаб точки
    [SerializeField] private DrawPhysicsLine trailPrefab; // префаб объекта с трейлом
    [SerializeField] private string portName = "COM3";
    
    private const int MAX_BUF = 230400;
    private const byte PKG_HEADER_0 = 0xA5;
    private const byte PKG_HEADER_1 = 0x5A;
    private const int MIN_PAYLOAD = 58;
    private const int POINT_PER_PACK = 16;

    private bool Within(int angle, int distance) => InRange(angleStart, angleEnd, angle) && distance <= maxDistance;
    
    private int angleStart, angleEnd, distanceStart, maxDistance;
    private List<GameObject> displayedObjects = new();
    private DrawPhysicsLine point;
    private SerialPort serial;
    private RaycastHit2D hit;
    private Task scanTask;
    
    [HideInInspector] public bool shutdown;
    
    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
            shutdown = true;
            OpenPort();
        }
        else if (instance == this)
            Destroy(gameObject);
    }
    
    public void Connect()
    {
        DestroyDisplayObjs();
        
        shutdown = false;
        if (CalibrationController.instance.HaveCalibration()) // получаем калибровку (реализация на ваше усмотрение)
        {
            angleStart = CalibrationController.instance.GetAngleStart();
            angleEnd = CalibrationController.instance.GetAngleEnd();
            maxDistance = CalibrationController.instance.GetMaxDistance();
        }
        
        scanTask = new Task(ScanTask);
        scanTask.Start();
    }
    
    public void ReConnect()
    {
        CloseConnection();
        Connect();
    }
    
    private void OpenPort()
    {
        try
        {
            serial = new SerialPort(portName, MAX_BUF);
            serial.Open();
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            throw;
        }
    }
    
    private void ScanTask()
    {
        // point = Instantiate(trailPrefab); // для отрисовки облака точек одним объектом с трейлом
        while (!shutdown)
        {
            List<byte> data = new List<byte>();
            
            while (data.Count < MIN_PAYLOAD)
                while (serial.BytesToRead > 0)
                    data.Add((byte)serial.ReadByte());
            
            if (data.Count >= MIN_PAYLOAD)
            {
                int start = 0;
                while (start < data.Count - 2)
                {
                    if (data[start] == PKG_HEADER_0 && data[start + 1] == PKG_HEADER_1)
                    {
                        byte[] payload = data.GetRange(start, MIN_PAYLOAD).ToArray();
                        
                        int startAngle = payload[5] * 256 + payload[6];
                        int endAngle = payload[55] * 256 + payload[56];
                        
                        List<(int, int)> finalData = new List<(int, int)>();
                        float diff = (endAngle + 36000 - startAngle) % 36000 / (POINT_PER_PACK - 1) / 100;
                        float _startAngle = startAngle / 100;
                        
                        for (int i = 0; i < POINT_PER_PACK; i++)
                        {
                            int o = 7 + i * 3;
                            finalData.Add((Mathf.RoundToInt(_startAngle + diff * i) % 360, payload[o] * 256 + payload[o + 1]));
                        }
                        
                        data.RemoveRange(0, MIN_PAYLOAD);
                        
                        // чтобы избежать проблем с потоками, необходимо использовать UnityMainThreadDispatcher
                        UnityMainThreadDispatcher.instance.Enqueue(isCalibrate ? () => Calibrate(finalData) : () => Game(finalData));
                        // UnityMainThreadDispatcher.instance.Enqueue(() => CreateObjs(finalData));
                        // nityMainThreadDispatcher.instance.Enqueue(() => CreateTrail(finalData));
                    }
                    
                    start++;
                }
            }
        }
    }
    
    private void Game(List<(int, int)> finalData) // в игре
    {
        DestroyDisplayObjs();
        foreach (var item in finalData)
        {
            int angle = item.Item1;
            int distance = item.Item2;
            Vector3 objPos = CreateObjPos(angle, distance);
            if (!Within(angle, distance))
                continue;
            
            hit = Physics2D.Raycast(Vector2.up, objPos, distance);
            if (hit)
                displayedObjects.Add(Instantiate(pointPrefab, objPos, Quaternion.identity));
        }
    }
    
    private void Calibrate(List<(int, int)> finalData) // для калибровки
    {
        DestroyDisplayObjs();
        foreach (var item in finalData)
        {
            int angle = item.Item1;
            int distance = item.Item2;
            Vector3 objPos = CreateObjPos(angle, distance);
            hit = Physics2D.Raycast(Vector2.up, objPos, distance);
            if (hit)
                displayedObjects.Add(Instantiate(pointPrefab, objPos, Quaternion.identity));
        }
    }
    
    #region Alternative
    private void CreateObjs(List<(int, int)> finalData) // в случае работы с точками
    {
        DestroyDisplayObjs();
        foreach (var item in finalData)
            displayedObjects.Add(Instantiate(pointPrefab, CreateObjPos(item.Item1, item.Item2), Quaternion.identity));
    }

    private void CreateTrail(List<(int, int)> finalData) // в случае работы с трейлом
    {
        foreach (var item in finalData)
        {
            point.UpdatePos();
            var objPos = CreateObjPos(item.Item1, item.Item2);
            point.transform.position = objPos;
            point.endPos = objPos;
            point.AddColliderToLine();
        }
    }
    #endregion

    private Vector3 CreateObjPos(int angle, int distance)
    {
        Vector3 objPos = PolarToRect(angle, distance);
        objPos = Camera.main.ScreenToWorldPoint(new Vector3(objPos.x, objPos.y));
        objPos = new Vector3(objPos.x, objPos.y, 0f);
        return objPos;
    }
    
    private Vector2 PolarToRect(float angle, float distance) // полярные координаты в декартовы
    {
        float x = -distance * Mathf.Cos(angle * Mathf.Deg2Rad);
        float y = distance * Mathf.Sin(angle * Mathf.Deg2Rad);
        
        return new Vector2(x, y);
    }

    #region RangeCheck
    private bool InRange(int start, int end, int x) // проверка на попадание угла в пределы
    {
        start = Normalize(0, 360, start);
        end = Normalize(0, 360, end);
        x = Normalize(0, 360, x);

        if (start <= end)
            return start <= x && x <= end;

        return !(end < x && x < start);
    }
    
    private int Normalize(int left, int right, int x)
    {
        int period = right - left;
        int temp = Mod(x, period);
        
        if (temp < left)
            return temp + period;
        if (temp >= right)
            return temp - period;
        
        return temp;
    }
    
    private int Mod(int x, int n)
    {
        if (x < 0)
            return -Mod(-x, n);
        if (n < 0)
            return Mod(x, -n);

        int m = x / n;
        return x - m * n;
    }
    #endregion
    
    private void DestroyDisplayObjs()
    {
        foreach (var obj in displayedObjects)
            Destroy(obj);

        displayedObjects.Clear();
    }
    
    private void CloseConnection()
    {
        shutdown = true;
        serial.Dispose();
        serial.Close();
    }
    
    void OnDestroy()
    {
        CloseConnection();
    }

    #region Debug
    private void DebugDraw(float angle, float distance) // отрисовка лучей (работает некорректно)
    {
        Ray2D ray = new Ray2D(Vector2.up, PolarToRect(angle, distance));
        Debug.DrawRay(ray.origin, ray.direction * 10, Color.red, 1.0f);
    }
    #endregion
}